# rxpdf

A wrapper around xpdf for R

## Introduction
Known to a lot of Linux users, `xpdf` is a collection of pdf utilities that
provides among other features an excellent text extractor. The `rxpdf` package 
provides bindings to the software so that the user does not need to leave the R 
workflow in order to run xpdf.
This software works especially well on multi-column or boxed text such as in newspapers. 

## Installation

#### Executables
In order to use the package the xpdf executables must be located in your `$PATH`.
If you are on a UNIX-like operating system, you can download it by running one 
of the following commands in your shell.

###### MacOS
```bash
brew install xpdf
```
###### Arch
```bash
sudo pacman -S xpdf
```

###### Debian
```bash
sudo apt install xpdf
```

###### REHL / Fedora
```bash
sudo dnf install xpdf
```

#### Package
The second and last step is to install the R package using `devtools`. 
Run the following command form your R console:
```r
# install.packages('devtools')
devtools::install_gitlab('giovannikraushaar/rxpdf')
```


## Motivation
Frustrated by the fact that all the pdf text extractors to my knowledge, 
`pdftools` and `Rpoppler` (despite being both based on `poppler` library which
is itself based on `xpdf` version 3), were only able to read text *as presented*,
and completly **unable** to distingush between textboxes inside a pdf, I found 
external utilities the only viable alternatives.

`rxpdf` might be a bit slower than `pdftools` but outputs far better results.

---

###### Disclaimer
xpdf is an open source software released by 
[Glyph & Cog, LLC](https://www.xpdfreader.com/download.html). 
All credits go to them. Current version is 4.02.


###### Tags
R, xpdf, pdf, text extraction
